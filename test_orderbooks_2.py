import asyncio

import bsmp
import btfn
import gdax
import krkn

g1 = gdax.API('/Users/areste/gapfinder/gdax.key', 'BTCEUR', 0.5, 0.0025, 'auto')
b1 = btfn.API('/Users/areste/gapfinder/bitfinex.key', 'BTCEUR', 0.5, 0.0025, 'auto')
bs = bsmp.API('/Users/areste/gapfinder/bitstamp.key', 'BTCEUR', 0.5, 0.0025, 'auto')
k1 = krkn.API('/Users/areste/gapfinder/kraken.key', 'BTCEUR', 0.5, 0.0025, 'auto')

asyncio.ensure_future(g1.start_balance_refresh(True))
asyncio.ensure_future(b1.start_balance_refresh(True))
asyncio.ensure_future(bs.start_balance_refresh(True))
asyncio.ensure_future(k1.start_balance_refresh(True))


async def main_loop():
    while True:
        await asyncio.sleep(60)

asyncio.get_event_loop().run_until_complete(main_loop())
