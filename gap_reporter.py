import requests, json


class GapReporter:

    def __init__(self, chatid, botid, name):

        self._entrypoint = 'https://api.telegram.org/'
        self._botid = botid
        self._chatid = chatid
        self._name = name
        self._update_id = 1

    def __call__(self, message):

        requests.post(self._entrypoint + 'bot' + self._botid + '/sendMessage',
                      data={'chat_id': self._chatid, 'text': '#' + self._name + '>\n' + message})
        return None

    def get_updates(self):

        text = 'noop'

        try:
            r = requests.post(self._entrypoint + 'bot' + self._botid + '/getUpdates',
                              data={'offset': self._update_id})
            response = json.loads(r.text)
            for update in response['result']:
                txt = 'message'
                if 'edited_message' in update:
                    txt = 'edited_message'
                cid = update[txt]['chat']['id']
                if str(cid) == self._chatid:
                    self._update_id = update['update_id'] + 1
                    text = update[txt]['text']
            return text

        except Exception as e:
            return text
