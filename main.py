import ast
import asyncio
import configparser
import importlib
import sys

import log_helper
import oracle
import stats_helper
from exchange import OrderbookNotRefreshedException
from oracle import UnexpectedCryptoBalanceException

#logging.basicConfig(level=logging.DEBUG)

c = configparser.RawConfigParser()
c.read(sys.argv[1])

log = log_helper.LogHelper(c['LogSection'])
simulation = ast.literal_eval(c.get('RuntimeSection', 'simulation'))
sleep_after_op = int(c.get('RuntimeSection', 'sleep.after.op'))
loop_timer = float(c.get('RuntimeSection', 'loop'))
ticker = c.get('RuntimeSection', 'ticker')
crypto = ticker[:3]
fiat = ticker[3:]
coin_min = float(c.get('RuntimeSection', 'coin.min'))
ob_refresh = c.get('RuntimeSection', 'orderbook.refresh')

xlist = []
for xname in str(c['RuntimeSection']['exchange.pair']).split(','):
    class_ = getattr(importlib.import_module(xname.lower()), 'API')
    xlist.append(class_(c[xname]['keys.file.path'], ticker, float(c[xname]['target.sell']), float(c[xname]['fee']), c[xname]['balance']))

sts = stats_helper.GapStats(c['LogSection']['log.folder'], xlist[0].label, xlist[1].label, ticker)

o = oracle.MarketOracle(xlist, coin_min, log, sts, simulation)
asyncio.ensure_future(o.bot_subscription(c, sys.argv[1]))


for x in xlist:
    asyncio.ensure_future(x._orderbook_refresh_ws())
    asyncio.ensure_future(x.start_balance_refresh(simulation))


async def main_loop():

    await asyncio.sleep(5)
    acc_earn = 0

    log('***** Initializing GapSocket *****', to_bot=True)
    log('***** Simulation: ' + str(simulation) + ' *****', to_bot=True)
    log('Interval check = ' + str(loop_timer) + 's', to_bot=True)
    log('Min crypto to sell = ' + str(coin_min), to_bot=True)
    log('Sleep after op = ' + str(sleep_after_op) + ' seconds', to_bot=True)
    for x in xlist:
        log(x.label + ': ' + x.print(), to_bot=True)

    while True:

        await asyncio.sleep(loop_timer)
        #o.update_stats()
        for x in xlist:
            print(x.label, x.orderbook)

        try:

            if o.seek_operation():

                o.execute_operation()
                await asyncio.sleep(sleep_after_op)
                diff = o.compute_balance_difference()
                acc_earn += diff

                for w in xlist:
                    log(w.label + ' balance: ' + str(w.balance), to_bot=True)

                log('Real earning: ' + str(diff) + ' ' + fiat, to_bot=True)
                log('Acc. earning: ' + str(acc_earn) + ' ' + fiat, to_bot=True)

                if not simulation:
                    o.register_earn(fiat)

        except UnexpectedCryptoBalanceException as e:
            log('** Fatal exception, ending program ** ' + str(e), to_bot=True)
            break
        except OrderbookNotRefreshedException as e:
            log('** Restarting orderbook refresh for ' + str(e) + ' **', to_bot=True)
            for f in xlist:
                if f.label == str(e):
                    f.task_ob_refresh.cancel()
                    f.task_ob_refresh = asyncio.ensure_future(f.start_orderbook_refresh())
                    await asyncio.sleep(5)

loop = asyncio.get_event_loop()
loop.run_until_complete(main_loop())
