from datetime import datetime
from datetime import timedelta
import sqlite3, numpy, sys


class GapStats:

    def __init__(self, db_folder, x0, x1, ticker):

        self._con = sqlite3.connect(db_folder + sys.argv[1] + '.db')
        self._cursor = self._con.cursor()
        self._table = x0 + x1 + '_' + ticker

        self._cursor.execute('CREATE TABLE IF NOT EXISTS ' + self._table +
                             ' (id DATE NOT NULL PRIMARY KEY, sell_' + x0 + ' REAL, sell_' + x1 + ' REAL)')
        self._cursor.execute('CREATE TABLE IF NOT EXISTS earnings (id DATE NOT NULL PRIMARY KEY, earned REAL, '
                             'unit CHAR(3))')

        self._con.commit()

    def insert_gaps(self, id, g0, g1):

        try:
            self._con.execute('INSERT INTO ' + self._table + ' VALUES (?,?,?)', (id, g0, g1))
            self._con.commit()
            return

        except sqlite3.Error as e:
            if self._con:
                self._con.rollback()

    def insert_earn(self, id, earn, unit):

        try:
            self._con.execute('INSERT INTO earnings VALUES (?,?,?)', (id, earn, unit))
            self._con.commit()
            return

        except sqlite3.Error as e:
            if self._con:
                self._con.rollback()

    def get_earn(self):

        query = 'SELECT SUM(earned), unit FROM earnings GROUP BY unit;'

        try:
            self._cursor.execute(query)
            rows = self._cursor.fetchall()
            return str(rows)

        except sqlite3.Error as e:
            if self._con:
                self._con.rollback()
            return None

    def run_query(self, query):

        try:
            self._cursor.execute(query)
            rows = self._cursor.fetchall()
            self._con.commit()
            return rows

        except sqlite3.Error as e:
            if self._con:
                self._con.rollback()
            return None

    def dump_stats(self):

        response = ''

        (exchanges, ticker) = str(self._table).split('_')
        exs = [exchanges[:4], exchanges[4:]]

        for h in [0.016, 3, 6, 12, 24, 48]:

            now = str(datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f'))
            then = str((datetime.now() - timedelta(hours=h)).strftime('%Y-%m-%d %H:%M:%S.%f'))

            if h > 1:
                response += '\n' + 'Last ' + str(h) + 'h\n'
            else:
                response += '\n' + 'Last ' + str(round(h*60)) + 'm\n'

            for x in exs:

                s = self.run_query('SELECT sell_' + x + ' from ' + self._table + ' WHERE id BETWEEN \'' + then + '\' AND \'' + now + '\';')

                try:
                    response += x + ' (perc. 99)   : ' + str(round(numpy.percentile(s, 99), 2)) + '\n'
                    response += x + ' (max value)  : ' + str(round(numpy.amax(s), 2)) + '\n'

                except Exception as e:
                    return 'Error getting stats'

        return response
