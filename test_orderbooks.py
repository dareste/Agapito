import asyncio
import btfn

#g1 = gdax.API('/Users/areste/gapfinder/gdax.key', 'BTCEUR', 0.5, 0.0025)
b1 = btfn.API('/Users/areste/gapfinder/bitfinex.key', 'BTCEUR', 0.5, 0.0025, 'auto')
#bs = bsmp.API('/Users/areste/gapfinder/bitstamp.key', 'BTCEUR', 0.5, 0.0025)

#asyncio.ensure_future(g1.start_orderbook_refresh())
asyncio.ensure_future(b1.start_orderbook_refresh())
#asyncio.ensure_future(bs.start_orderbook_refresh())

async def main_loop():
    await asyncio.sleep(60)
    #g2 = gdax.API('/Users/areste/gapfinder/gdax.key', 'BTCEUR', 0.5, 0.0025)
    #b2 = btfn.API('/Users/areste/gapfinder/bitfinex.key', 'BTCEUR', 0.5, 0.0025)
    #asyncio.ensure_future(g2.start_orderbook_refresh())
    #asyncio.ensure_future(b2.start_orderbook_refresh())
    #await asyncio.sleep(10)
    #print('OLD ', b1.orderbook_validity, b1.orderbook)
    #print('NEW ', b2.orderbook_validity, b2.orderbook)
    #print('OLD ', g1.orderbook)
    #print('NEW ', g2.orderbook)
    #print('BSMP ', bs.orderbook)

asyncio.get_event_loop().run_until_complete(main_loop())
