import copy, time


class OrderbookNotRefreshedException(Exception):
    pass

class API:

    def __init__(self, key_path, ticker, target_sell, fee, balance):

        self._key_path = key_path
        self.target_sell = target_sell
        self.max_crypto_sell = 0
        self.label = ''
        self.ticker = ticker
        self.fee = fee
        self.task_ob_refresh = None
        self.task_ba_refresh = None

        if self.target_sell > 0:
            self.sell_all = False
        else:
            self.sell_all = True

        self.orderbook = {'bids': [], 'asks': []}
        self.orderbook_validity = 0
        self.balance_src = balance if balance == 'auto' else 'manual'
        if self.balance_src == 'auto':
            self.balance = {self.ticker[:3]: 0.0, self.ticker[3:]: 0.0}
        else:
            self.balance = {self.ticker[:3]: float(str(balance).split(',')[0]), self.ticker[3:]: float(str(balance).split(',')[1])}
        self.balance_snapshot = {}

    def print(self):
        return 'target_sell=' + str(self.target_sell) + ', balance=' + str(self.balance)

    def update_gap(self, new_target_sell):
        self.target_sell = new_target_sell

    async def start_orderbook_refresh(self, method):

        if method == 'WS':
            self._orderbook_refresh_ws()

        else:
            self._orderbook_refresh_rest()

    async def _orderbook_refresh_ws(self):
        raise NotImplementedError("Please Implement this method")

    async def _orderbook_refresh_rest(self):
        raise NotImplementedError("Please Implement this method")

    async def start_balance_refresh(self, simulation):
        raise NotImplementedError("Please Implement this method")

    def set_order(self, side, mode, qty, price):
        raise NotImplementedError("Please Implement this method")

    def take_balance_snapshot(self):
        self.balance_snapshot = copy.deepcopy(self.balance)

    def calculate_price(self, op, amount):

        if int(round(time.time() * 1000)) - self.orderbook_validity > 60000:
            raise OrderbookNotRefreshedException(self.label)

        if 'sell' in op:
            orders = self.orderbook['bids']
        else:
            orders = self.orderbook['asks']

        cost = 0
        i = 0
        remaining = amount

        try:
            while remaining > 0:
                if remaining <= float(orders[i][1]):
                    cost += remaining * float(orders[i][0])
                    remaining = 0

                else:
                    cost += float(orders[i][1]) * float(orders[i][0])
                    remaining -= float(orders[i][1])
                    i += 1

            return cost

        except Exception as e:

            return -1
