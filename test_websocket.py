import asyncio
import websockets
import json
import copy


async def consumer(message, orderbook):

    m = json.loads(message)
    print("RECV> {}".format(m))

    if m['type'] == 'snapshot':
        orderbook = copy.deepcopy(m)

    if m['type'] == 'l2update':
        for change in m['changes']:
            i = 0
            side = 'bids'
            if change[0] == 'sell':
                side = 'asks'
            for element in orderbook[side]:
                price_book = float(element[0])
                price_update = float(change[1])
                if price_book == price_update:
                    if change[2] == '0':
                        orderbook[side].remove(element)
                    else:
                        element[1] = change[2]
                    break
                if ((price_book < price_update) and side == 'bids') or ((price_book > price_update) and side == 'asks'):
                    orderbook[side].insert(i, [change[1], change[2]])
                    break
                i += 1

    return orderbook

async def w1():
    # orderbook = {}
    async with websockets.connect('wss://ws-feed.gdax.com') as websocket1:

        name = '{"type": "subscribe","product_ids": ["BTC-EUR"],"channels": ["level2","heartbeat"]}'
        await websocket1.send(name)
        print("SEND-GDAX> {}".format(name))

        async for message in websocket1:
            #orderbook = await consumer(message, orderbook)
            #print('ORDERBOOK> ', orderbook)
            print('RECV-GDAX> ', message)

async def w2():
    # orderbook = {}
    async with websockets.connect('wss://api.bitfinex.com/ws') as websocket2:

        name = '{"event":"subscribe","channel":"book","pair":"BTCEUR","prec":"P0","length":"25","freq":"F0"}'
        await websocket2.send(name)
        print("SEND-BTFN> {}".format(name))

        async for message in websocket2:
            #orderbook = await consumer(message, orderbook)
            #print('ORDERBOOK> ', orderbook)
            print('RECV-BTFN> ', message)

async def main():
    asyncio.ensure_future(w1())
    asyncio.ensure_future(w2())
    await asyncio.sleep(10)


asyncio.get_event_loop().run_until_complete(main())
