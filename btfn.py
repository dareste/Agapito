import asyncio
import base64
import copy
import exchange
import hashlib
import hmac
import json
import requests
import time
import websockets


class API(exchange.API):

    def __init__(self, key_path, ticker, target_sell, fee, balance):

        exchange.API.__init__(self, key_path, ticker, target_sell, fee, balance)

        f = open(self._key_path, "r")
        lines = f.readlines()
        self._apikey = lines.__getitem__(0).strip()
        self._apikeysecret = lines.__getitem__(1).strip()
        self.label = 'BTFN'
        self._entrypoint = 'https://api.bitfinex.com'

        self.chan_id = 0
        self.version = 0

    def set_order(self, side, mode, qty, price):

        payload = {'symbol': self.ticker, 'amount': str(qty), 'price': str(price), 'side': side}
        if mode == 'limit':
            payload['type'] = 'exchange limit'
        else:
            payload['type'] = 'exchange market'

        try:
            return self._auth_call('/v1/order/new', payload)

        except requests.exceptions.RequestException as err:
            print(err)
            return -1

    def _auth_call(self, uri, payload):

        payload['request'] = uri
        payload['nonce'] = str(time.time() * 1000000)
        h = self._sign(payload)

        try:
            r = requests.post(self._entrypoint + uri, data=payload, headers=h)
        except requests.exceptions.RequestException as err:
            raise err

        try:
            data = json.loads(r.text)
            return data
        except json.JSONDecodeError as err:
            print('Error al parsear el Json: ' + str(err))
            return -1

    def _sign(self, payload_object):

        payload_json = json.dumps(payload_object)
        payload = base64.standard_b64encode(payload_json.encode('utf8'))

        m = hmac.new(self._apikeysecret.encode('utf8'), payload, hashlib.sha384)
        sig = m.hexdigest()

        headers = {
            'X-BFX-APIKEY': self._apikey,
            'X-BFX-PAYLOAD': payload,
            'X-BFX-SIGNATURE': sig
        }

        return headers

    async def start_balance_refresh(self, simulation):

        crypto = self.ticker[3:]
        fiat = self.ticker[:3]

        while True and self.balance_src == 'auto':

            try:
                r = self._auth_call('/v1/balances', {})
                for entry in r:
                    if entry['currency'] == str(crypto).lower():
                        self.balance[crypto] = float(entry['available'])
                    if entry['currency'] == str(fiat).lower():
                        self.balance[fiat] = float(entry['available'])

            except Exception as e:
                print('Error reading balance')

            print(self.balance)
            await asyncio.sleep(60)
            if simulation:
                break

    async def _orderbook_refresh_rest(self):

        while True:

            new_orderbook = {'bids': [], 'asks': []}
            asyncio.sleep(1)

            try:
                r = requests.get(self._entrypoint + '/v1/book/' + self.ticker)

                bids = json.loads(r.text)['bids']
                for bid in bids:
                    new_orderbook['bids'].append([float(bid['price']), float(bid['amount'])])

                asks = json.loads(r.text)['asks']
                for ask in asks:
                    new_orderbook['asks'].append([float(ask['price']), float(ask['amount'])])

                self.orderbook = copy.deepcopy(new_orderbook)
                self.orderbook_validity = int(round(time.time() * 1000))

            except Exception as e:
                print(e)

    async def _orderbook_refresh_ws(self):

        try:

            async with websockets.connect('wss://api.bitfinex.com/ws/2') as websocket:
                name = '{"event":"subscribe","channel":"book","symbol":"' + self.ticker + \
                       '","prec":"P0","freq":"F0","length":"100","freq":"F0"}'
                await websocket.send(name)

                async for message in websocket:

                    m = json.loads(message)
                    self.orderbook_validity = int(round(time.time() * 1000))
                    print('BTFN: ' + str(m))

                    if 'event' in m:
                        if m['event'] == 'info':
                            self.version = m['version']
                        if m['event'] == 'subscribed':
                            self.chan_id = m['chanId']

                    if 'event' not in m:
                        if len(m) == 2 and len(m[1]) != 3 and m[0] == self.chan_id and m[1] != 'hb':
                            for line in m[1]:
                                if line[2] < 0:
                                    self.orderbook['asks'].append([line[0], -line[2]])
                                else:
                                    self.orderbook['bids'].append([line[0], line[2]])

                        else:
                            if m[1] != 'hb':
                                update = m[1]
                                i = 0
                                side = 'bids' if update[2] > 0 else 'asks'
                                m_price = update[0]
                                m_count = update[1]
                                m_amount = abs(update[2])
                                op = False

                                for element in self.orderbook[side]:
                                    b_price = element[0]

                                    if b_price == m_price:
                                        op = True
                                        if m_count == 0:
                                            self.orderbook[side].remove(element)
                                        else:
                                            element[1] = m_amount
                                        break

                                    if ((b_price < m_price) and side == 'bids') or (
                                                (b_price > m_price) and side == 'asks'):
                                        self.orderbook[side].insert(i, [m_price, m_amount])
                                        break
                                    i += 1

                                if len(self.orderbook[side]) == i and op is False:
                                    self.orderbook[side].append([m_price, m_amount])

            print('Messages are over')

        except Exception as e:
            print(e)
