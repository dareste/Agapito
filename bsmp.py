import asyncio
import copy
import exchange
import hashlib
import hmac
import json
import pusherclient
import requests
import time


class API(exchange.API):

    def __init__(self, key_path, ticker, target_sell, fee, balance):

        exchange.API.__init__(self, key_path, ticker, target_sell, fee, balance)

        f = open(self._key_path, "r")
        lines = f.readlines()
        self._apikey = lines.__getitem__(0).strip()
        self._apikeysecret = lines.__getitem__(1).strip()
        self._cust_id = lines.__getitem__(2).strip()
        self.label = 'BSMP'
        self._entrypoint = 'https://www.bitstamp.net'
        self._websocket_key = 'de504dc5763aeef9ff52'
        self._pusher = pusherclient.Pusher(self._websocket_key)

    def connect_handler(self, data):

        order_book_channel = self._pusher.subscribe('order_book_' + str(self.ticker).lower())
        order_book_channel.bind('data', self.order_book_callback)

    def order_book_callback(self, data):

        new_orderbook = {'bids': [], 'asks': []}
        m = json.loads(data)
        print('BSMP: ' + str(m))
        self.orderbook_validity = int(round(time.time() * 1000))

        for side in ['bids', 'asks']:
            for item in m[side]:
                new_orderbook[side].append([float(item[0]), float(item[1])])

        self.orderbook = copy.deepcopy(new_orderbook)

    async def _orderbook_refresh_rest(self):

        while True:

            new_orderbook = {'bids': [], 'asks': []}
            asyncio.sleep(1)

            try:

                r = requests.get(self._entrypoint + '/api/v2/order_book/' + str(self.ticker).lower())
                bids = json.loads(r.text)['bids']
                for bid in bids:
                    new_orderbook['bids'].append([float(bid[0]), float(bid[1])])

                asks = json.loads(r.text)['asks']
                for ask in asks:
                    new_orderbook['asks'].append([float(ask[0]), float(ask[1])])

                print('BSMP: ' + str(new_orderbook))
                self.orderbook = copy.deepcopy(new_orderbook)
                self.orderbook_validity = int(round(time.time() * 1000))

            except Exception as e:
                print(e)

    async def _orderbook_refresh_ws(self):

        try:

            self._pusher.connection.bind('pusher:connection_established', self.connect_handler)
            self._pusher.connect()

        except Exception as e:
            raise e

    def _auth_call(self, uri, payload):

        nonce = int(time.time() * 1000000)
        message = str(nonce) + self._cust_id + self._apikey
        signature = hmac.new(
            self._apikeysecret.encode('utf8'), msg=message.encode('utf8'),
            digestmod=hashlib.sha256).hexdigest().upper()

        payload['key'] = self._apikey
        payload['signature'] = signature
        payload['nonce'] = nonce

        try:
            r = requests.post(self._entrypoint + uri, data=payload)
        except requests.exceptions.RequestException as err:
            raise err

        try:
            data = json.loads(r.text)
            return data
        except json.JSONDecodeError as err:
            print('Error al parsear el Json: ' + str(err))
            return -1

    async def start_balance_refresh(self, simulation):

        crypto = self.ticker[3:]
        fiat = self.ticker[:3]

        while True and self.balance_src == 'auto':

            try:
                r = self._auth_call('/api/v2/balance/', {})
                self.balance[crypto] = float(r[str(crypto).lower() + '_available'])
                self.balance[fiat] = float(r[str(fiat).lower() + '_available'])

            except Exception as e:
                print('Error reading balance')

            print(self.balance)
            await asyncio.sleep(60)
            if simulation:
                break

    def set_order(self, side, mode, qty, price):

        uri = '/api/v2/'
        if mode == 'market':
            uri += side + '/market/' + str(self.ticker).lower() + '/'
            payload = {'amount': str(qty)}
        else:
            uri += side + '/' + str(self.ticker).lower() + '/'
            payload = {'amount': str(qty), 'price': str(price)}

        try:
            return self._auth_call(uri, payload)

        except requests.exceptions.RequestException as err:
            print(err)
            return -1
