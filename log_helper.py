import logging, sys, aiohttp, threading
from gap_reporter import GapReporter


class LogHelper:

    def __init__(self, log_section_dict):

        self._logger = logging.getLogger(log_section_dict['log.name'])
        self._hdlr = logging.FileHandler(log_section_dict['log.folder'] + sys.argv[1] + '.log')
        self._formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self._hdlr.setFormatter(self._formatter)
        self._logger.addHandler(self._hdlr)
        self._logger.setLevel(logging.INFO)
        self._bot = GapReporter(log_section_dict['bot.chat'], log_section_dict['bot.id'], log_section_dict['bot.name'])


    def __call__(self, message, to_bot):

        self._logger.info(message)
        if to_bot is True:
            thread = threading.Thread(target=self._bot(message), args=())
            thread.daemon = True  # Daemonize thread
            thread.start()
            #self._bot(message)

    def get_bot(self):

        return self._bot
