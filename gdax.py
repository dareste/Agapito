import asyncio
import base64
import copy
import exchange
import hashlib
import hmac
import json
import requests
import time
import websockets
from requests.auth import AuthBase


class API(AuthBase, exchange.API):

    def __init__(self, key_path, ticker, target_sell, fee, balance):

        exchange.API.__init__(self, key_path, ticker, target_sell, fee, balance)

        f = open(key_path, "r")
        lines = f.readlines()
        self._api_key = lines.__getitem__(0).strip()
        self._secret_key = lines.__getitem__(1).strip()
        self._passphrase = lines.__getitem__(2).strip()
        self.label = 'GDAX'
        self._entrypoint = 'https://api.gdax.com'
        self._gdax_ticker = self.ticker[:3] + '-' + self.ticker[3:]

    def __call__(self, request):

        timestamp = str(time.time())
        message = timestamp + request.method + request.path_url + (request.body or '')
        message = message.encode('ascii')
        hmac_key = base64.b64decode(self._secret_key)
        signature = hmac.new(hmac_key, message, hashlib.sha256)
        signature_b64 = base64.b64encode(signature.digest())

        request.headers.update({
            'CB-ACCESS-SIGN': signature_b64,
            'CB-ACCESS-TIMESTAMP': timestamp,
            'CB-ACCESS-KEY': self._api_key,
            'CB-ACCESS-PASSPHRASE': self._passphrase,
            'Content-Type': 'application/json'
        })

        return request

    def set_order(self, side, mode, qty, price):

        order = {}

        if mode == 'market':
            order = {'size': qty, 'type': mode, 'side': side, 'product_id': self._gdax_ticker}
        if mode == 'limit':
            order = {'type': mode, 'size': qty, 'side': side, 'product_id': self._gdax_ticker, 'price': price}

        try:
            r = requests.post(self._entrypoint + '/orders', data=json.dumps(order), auth=self)
            return r

        except requests.exceptions.RequestException as err:
            print(err)
            return -1

    async def start_balance_refresh(self, simulation):

        crypto = self.ticker[3:]
        fiat = self.ticker[:3]

        while True and self.balance_src == 'auto':

            try:
                r = requests.get(self._entrypoint + '/accounts', auth=self)
                data = json.loads(r.text)
                for entry in data:
                    if entry['currency'] == str(crypto):
                        self.balance[crypto] = float(entry['available'])
                    if entry['currency'] == str(fiat):
                        self.balance[fiat] = float(entry['available'])

            except Exception as e:
                print('Error reading balance')

            print(self.balance)
            await asyncio.sleep(60)
            if simulation:
                break

    async def _orderbook_refresh_rest(self):

        while True:

            asyncio.sleep(1)
            book = {'bids': [], 'asks': []}

            try:
                r = requests.get(self._entrypoint + '/products/' + self._gdax_ticker + '/book?level=2')
                for op in ['bids', 'asks']:
                    for entry in json.loads(r.text)[op]:
                        book[op].append([float(entry[0]), float(entry[1])])

                self.orderbook = copy.deepcopy(book)
                self.orderbook_validity = int(round(time.time() * 1000))

            except Exception as e:
                print(e)

    async def _orderbook_refresh_ws(self):

        try:

            async with websockets.connect('wss://ws-feed.gdax.com') as websocket:
                name = '{"type": "subscribe","product_ids": ["' + self._gdax_ticker + '"],"channels": ["level2","heartbeat"]}'
                await websocket.send(name)

                async for message in websocket:

                    print(message)
                    m = json.loads(message)
                    print('GDAX: ' + str(m))
                    self.orderbook_validity = int(round(time.time() * 1000))

                    if m['type'] == 'snapshot':
                        for side in ['bids', 'asks']:
                            for line in m[side]:
                                self.orderbook[side].append([float(line[0]), float(line[1])])

                    if m['type'] == 'l2update':
                        for change in m['changes']:
                            i = 0
                            side = 'asks' if change[0] == 'sell' else 'bids'
                            m_price = float(change[1])
                            m_amount = float(change[2])
                            op = False

                            for element in self.orderbook[side]:
                                b_price = element[0]

                                if b_price == m_price:
                                    op = True
                                    if m_amount == 0:
                                        self.orderbook[side].remove(element)
                                    else:
                                        element[1] = m_amount
                                    break
                                if ((b_price < m_price) and side == 'bids') or (
                                            (b_price > m_price) and side == 'asks'):
                                    self.orderbook[side].insert(i, [m_price, m_amount])
                                    break
                                i += 1

                            if len(self.orderbook[side]) == i and op is False:
                                self.orderbook[side].append([m_price, m_amount])

        except Exception as e:
            raise e
