import asyncio
import copy
import exchange
import json
import krakenex
import requests
import time


class API(exchange.API):

    def __init__(self, key_path, ticker, target_sell, fee, balance):

        exchange.API.__init__(self, key_path, ticker, target_sell, fee, balance)

        self.__krakenex__ = krakenex.API()
        self.__krakenex__.load_key(self._key_path)
        self.label = 'KRKN'
        self._entrypoint = _entrypoint = 'https://api.kraken.com'
        self._tickermap = {'BTC': 'XXBT', 'USD': 'ZUSD', 'EUR': 'ZEUR', 'BTCUSD': 'XXBTZUSD', 'LTCUSD': 'XLTCZUSD',
                       'BTCEUR': 'XXBTZEUR', 'BCHBTC': 'BCHXBT', 'ETHBTC': 'XETHXXBT', 'DSHUSD': 'DASHUSD',
                  'ETHEUR': 'XETHZEUR'}

    def _map_ticker(self, ticker):

        if ticker not in self._tickermap.keys():
            return ticker
        else:
            return self._tickermap[ticker]

    async def start_balance_refresh(self, simulation):

        crypto = self._map_ticker(self.ticker[3:])
        fiat = self._map_ticker(self.ticker[:3])

        while True and self.balance_src == 'auto':

            try:
                r = self.__krakenex__.query_private('Balance')
                self.balance = {self.ticker[3:]: float(r['result'][crypto]), self.ticker[:3]: float(r['result'][fiat])}

            except Exception as e:
                print('Error reading balance')

            print(self.balance)
            await asyncio.sleep(60)
            if simulation:
                break

    def set_order(self, side, mode, qty, price):

        ticker = self._map_ticker(self.ticker)

        payload = {'pair': ticker, 'type': side, 'ordertype': mode, 'volume': qty}
        if mode == 'limit':
            payload['price'] = str(price)

        try:
            r = self.__krakenex__.query_private('AddOrder', payload)
            print(r)
            return r

        except requests.exceptions.RequestException as err:
            print(str(err))
            return {'error': str(err)}

    async def _orderbook_refresh_rest(self):

        ticker = self._map_ticker(self.ticker)
        pairdata = {'pair': ticker}

        while True:

            asyncio.sleep(1)

            try:
                r = requests.post(self._entrypoint + '/0/public/Depth', data=pairdata)
                b = json.loads(r.text)
                book = {'bids': [], 'asks': []}

                for op in ['bids', 'asks']:
                    for entry in b['result'][ticker][op]:
                        book[op].append([float(entry[0]), float(entry[1])])
                print('KRKN: ' + str(book))
                self.orderbook = copy.deepcopy(book)
                self.orderbook_validity = int(round(time.time() * 1000))

            except Exception as e:
                print(e)

    async def _orderbook_refresh_ws(self):

        ticker = self._map_ticker(self.ticker)
        pairdata = {'pair': ticker}

        while True:

            try:
                r = requests.post(self._entrypoint + '/0/public/Depth', data=pairdata)
                b = json.loads(r.text)
                print('KRKN: ' + str(b))
                book = {'bids': [], 'asks': []}

                for op in ['bids', 'asks']:
                    for entry in b['result'][ticker][op]:
                        book[op].append([float(entry[0]), float(entry[1])])

                self.orderbook = copy.deepcopy(book)
                self.orderbook_validity = int(round(time.time() * 1000))

            except Exception as e:
                print(e)

            await asyncio.sleep(1)
