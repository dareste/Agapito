import asyncio
import itertools
from datetime import datetime
from typing import List

import exchange
from log_helper import LogHelper
from stats_helper import GapStats


class UnexpectedCryptoBalanceException(Exception):
    pass


class MarketOracle:
    def __init__(self, x: List[exchange.API], coin_min, l: LogHelper, g: GapStats, simulation: bool):

        self._x = x
        self._optimal = {}
        self._log = l
        self._gap_db = g
        self._simulation = simulation
        self._coin_min = coin_min
        self._ticker = self._x[0].ticker
        self._crypto = self._x[0].ticker[:3]
        self._fiat = self._x[0].ticker[3:]

    async def bot_subscription(self, config_dict, config_file):

        bot = self._log.get_bot()

        while True:

            txt = str(bot.get_updates()).lower()

            if txt == '/ping':
                bot('pong')

            elif txt == '/earn':
                bot(self._gap_db.get_earn())

            elif txt.startswith('/gap'):
                try:

                    (_, label, value) = txt.split(' ')
                    for item in self._x:
                        if item.label == str(label).upper():
                            item.update_gap(float(value))
                            if item.target_sell > 0:
                                item.sell_all = False
                            else:
                                item.sell_all = True
                            bot(item.label + ' target_sell updated to ' + str(item.target_sell))
                            bot(item.label + ' sell_all updated to ' + str(item.sell_all))
                            config_dict.set(item.label, 'target.sell', str(item.target_sell))
                            with open(config_file, 'w') as configfile:
                                config_dict.write(configfile)

                except Exception as e:
                    bot('Wrong command.')

            elif txt == '/price':
                for x in self._x:
                    bot(x.label + ' ' + str(round(x.orderbook['bids'][0][0], 2)) + '-' +
                        str(round(x.orderbook['asks'][0][0], 2)) + ' t:' + str(x.orderbook_validity))

            elif txt == '/stats':
                bot(self._gap_db.dump_stats())

            elif txt == '/prop':
                for item in self._x:
                    bot(item.label + ' properties: ' + item.print())

            await asyncio.sleep(5)

    def compute_balance_difference(self):

        tot_old_fiat = 0
        tot_new_fiat = 0
        tot_old_crpt = 0
        tot_new_crpt = 0

        for x in [self._optimal['buy'], self._optimal['sell']]:

            tot_new_fiat += x.balance[self._fiat]
            tot_old_fiat += x.balance_snapshot[self._fiat]
            tot_new_crpt += x.balance[self._crypto]
            tot_old_crpt += x.balance_snapshot[self._crypto]

        if round(abs(tot_new_crpt - tot_old_crpt), 8) > 0:
            raise UnexpectedCryptoBalanceException(
                'Unexpected crypto balance difference: ' + str(round(abs(tot_new_crpt - tot_old_crpt), 8))
            )

        return round(tot_new_fiat - tot_old_fiat, 2)

    def seek_operation(self):

        peer_security_margin = 0.01
        optimal = []
        message = ''
        self._optimal = {}
        combos = [list(e) for e in itertools.combinations(self._x, 2)]
        for x_pair in combos: # Se van recorriendo los exchanges de a pares

            i = 0

            for x in x_pair: # Para cada uno de los exchanges en la tupla del par

                best_op = -1000000
                opt_temp = {}

                # Estimate max coins to sell
                not_enough_funds = True
                max_crypto_amount = x.balance[self._crypto] # Maxima cantidad de coins a vender, todos los coins.
                peer = x_pair[(i + 1) % 2] # Peer es la pareja del exchange en el que estamos ahora

                while not_enough_funds:

                    cost = peer.calculate_price('buy', max_crypto_amount)

                    if cost > peer.balance[self._fiat] * (1 - peer_security_margin): #Si no tenemos fondos sufucientes para comprar todos los coins se va reduciendo hasta encontrar el máximo de coins posibles para comprar
                        max_crypto_amount -= self._coin_min

                    else:
                        not_enough_funds = False

                max_coins_to_sell = max_crypto_amount if max_crypto_amount > 0 else 0

                # Estimate initial number of coins to sell
                coins_to_sell = self._coin_min


                if x.sell_all:
                    while coins_to_sell <= max_coins_to_sell:
                        coins_to_sell += self._coin_min
                    coins_to_sell -= self._coin_min if coins_to_sell > self._coin_min else 0

                # Seek operation
                while coins_to_sell <= max_coins_to_sell:

                    benefit = x.calculate_price('sell', coins_to_sell) * (1 - x.fee)
                    cost = peer.calculate_price('buy', coins_to_sell) * (1 + peer.fee)

                    if benefit != -1 and cost != -1:

                        gap = (benefit - cost) * 100 / benefit
                        if benefit - cost > best_op:
                            best_op = benefit - cost
                            opt_temp = {'sell': x, 'buy': peer, 'earn': round(benefit - cost, 4),
                                        'coins': round(coins_to_sell, 8), 'benefit': round(benefit, 4),
                                        'cost': round(cost, 4), 'gap': round(gap, 4)}

                    coins_to_sell += self._coin_min

                i += 1
                if opt_temp:
                    optimal.append(opt_temp)

        # Evaluate if there is a valid op

        if not optimal:
            self._log('Best Op: none. ' + message, to_bot=False)

        else:
            for o in optimal:
                #print(o)
                self._log('Best Op: Sell in ' + o['sell'].label + ' || Buy in ' + o['buy'].label + ' || ' +
                          'Coins ' + str(o['coins']) + ' ' + self._crypto + ' || ' +
                          'Earn ' + str(o['earn']) + ' ' + self._fiat + ' || ' +
                          'Gap ' + str(o['gap']) + '%', to_bot=False)

            for o in optimal:
                if o['gap'] > o['sell'].target_sell: #Si el gap es mayor a target
                    if self._optimal == {}:
                        self._optimal = o
                    elif o['gap'] > self._optimal['gap']: #Si ya hay un optimal pero este es mejor --> me quedo con este
                        self._optimal = o

            if self._optimal != {}: # Si hay optimal se devuelve True y se ejecutará la operacion
                return True
            else:
                return False

    def execute_operation(self):

        self._optimal['sell'].take_balance_snapshot()
        self._optimal['buy'].take_balance_snapshot()

        if self._simulation is False:

            self._optimal['sell'].set_order('sell', 'market', self._optimal['coins'], '5000')
            self._optimal['buy'].set_order('buy', 'market', self._optimal['coins'], '1')

        else:

            self._optimal['sell'].balance[self._crypto] -= self._optimal['coins']
            self._optimal['sell'].balance[self._fiat] += self._optimal['benefit']
            self._optimal['buy'].balance[self._crypto] += self._optimal['coins']
            self._optimal['buy'].balance[self._fiat] -= self._optimal['cost']

        self._log('** NEW Op ** Sell ' + str(self._optimal['coins']) + ' ' + str(self._crypto) + ' in ' +
                  self._optimal['sell'].label + ' || Buy in ' + self._optimal['buy'].label, to_bot=True)
        self._log('Estimated earning ' + str(self._optimal['earn']) + ' ' + str(self._fiat), to_bot=True)

    def update_stats(self):

        coins = 0
        gap = []
        i = 0

        for x in self._x:
            coins += x.balance[self._crypto]

        for x in self._x:

            peer = self._x[(i + 1) % 2]
            benefit = x.calculate_price('sell', coins) * (1 - x.fee)
            cost = peer.calculate_price('buy', coins) * (1 + peer.fee)
            gap.append(round((benefit - cost) * 100 / benefit, 2))
            i += 1

        self._gap_db.insert_gaps(str(datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')), gap[0], gap[1])

    def register_earn(self, earn):

        self._gap_db.insert_earn(str(datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')), earn, self._fiat)
