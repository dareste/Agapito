import ast
import asyncio
import configparser
import importlib
import sys
import time
from datetime import datetime

import log_helper
import oracle
import stats_helper

#logging.basicConfig(level=logging.DEBUG)

c = configparser.RawConfigParser()

c.read(sys.argv[1])

log = log_helper.LogHelper(c['LogSection'])
simulation = ast.literal_eval(c.get('RuntimeSection', 'simulation'))
sleep_after_op = int(c.get('RuntimeSection', 'sleep.after.op'))
loop_timer = float(c.get('RuntimeSection', 'loop'))
ticker = c.get('RuntimeSection', 'ticker')
crypto = ticker[:3]
fiat = ticker[3:]
coin_min = float(c.get('RuntimeSection', 'coin.min'))


xlist = []
for xname in str(c['RuntimeSection']['exchange.pair']).split(','):
    class_ = getattr(importlib.import_module(xname.lower()), 'API')
    xlist.append(class_(c[xname]['keys.file.path'], ticker, float(c[xname]['target.sell']), float(c[xname]['fee']), c[xname]['balance']))

sts = stats_helper.GapStats(c['LogSection']['log.folder'], xlist[0].label, xlist[1].label, ticker)

o = oracle.MarketOracle(xlist, coin_min, log, sts, simulation)
asyncio.ensure_future(o.bot_subscription(c, sys.argv[1]))


for x in xlist:
    x.start_balance_refresh(simulation)

def main_loop():
    try:
        time.sleep(5)
        acc_earn = 0
        ops = int(c.get('RuntimeSection', 'ops'))
        # If ops <= 0 then ops will be the maximum value a variable of type int can take.
        if ops <= 0:
            ops = sys.maxsize
        log('***** Initializing GapSocket *****', to_bot=True)
        log('***** Simulation: ' + str(simulation) + ' *****', to_bot=True)
        log('Interval check = ' + str(loop_timer) + 's', to_bot=True)
        log('Min crypto to sell = ' + str(coin_min), to_bot=True)
        log('Sleep after op = ' + str(sleep_after_op) + ' seconds', to_bot=True)
        log('Ops = ' + str(ops), to_bot=True)
        for x in xlist:
            log(x.label + ': ' + x.print(), to_bot=True)

        while True and ops > 0:

            time.sleep(loop_timer)
            start_request_time = datetime.now()
            for x in xlist:
                x.task_ob_refresh = x.get_orderbook_rest()
            end_request_time = datetime.now()
            delay = end_request_time - start_request_time
            print('Delay: '+str(delay.seconds))
            log('Delay: '+str(delay.seconds), to_bot=False)
            #o.update_stats()

            try:
                if delay.seconds <= round(xlist.__len__()/2,0):
                    log('Seeking operation.', to_bot=False)
                    print('Seeking operation.')
                    if o.seek_operation():

                        o.execute_operation()
                        ops -= 1
                        time.sleep(sleep_after_op)
                        for x in xlist:
                            x.start_balance_refresh(simulation)
                        diff = o.compute_balance_difference()
                        acc_earn += diff

                        for w in xlist:
                            log(w.label + ' balance: ' + str(w.balance), to_bot=True)

                        log('Real earning: ' + str(diff) + ' ' + fiat, to_bot=True)
                        log('Acc. earning: ' + str(acc_earn) + ' ' + fiat, to_bot=True)

                        if not simulation:
                            o.register_earn(diff)
                else:
                    log('Too much delay.', to_bot=False)
                    print('Too much delay.')
            except Exception as e:
                log('** Fatal exception, waiting 10 minute ** ' + str(e), to_bot=True)
                break

    except Exception as e:
        log('** EXCEPTION: ' + str(e) + ' **', to_bot=False)

main_loop()
